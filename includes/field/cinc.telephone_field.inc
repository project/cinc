<?php

/**
 * Creates address fields that can be saved to content types.
 *
 * @code
 * $my_field = CINC::init('TelephoneField')->set('field_name', 'my_field');
 * @endcode
 */
class CINC_TelephoneField extends CINC_Field {

  public static $dependencies = array('field', 'telephone');
  public static $field_types = array('telephone');

  function __construct() {

    parent::__construct();

    $this
      ->set('type', 'telephone')
      ->set('module', 'telephone')
      ->set_instance('widget', array(
        'type' => 'telephone_default',
        'module' => 'telephone',
        'settings' => array(),
      ));

  }

}
